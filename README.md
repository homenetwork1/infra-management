# infra-management

This project is to manage my personal homelab with ansible

## Project setup

- `pyenv virtualenv 3.11.4 infra-management-3-11-4`
- `pip install -r requirements.txt`

## Update packages

- `python -m piptools compile requirements.in`

## Docs

    see folder docs

## setup cloud

configire aws for terraform s3 backend
`aws configure`

place token in secret file

`echo 'hcloud_token="<token>"' > cloud/terraform.tfvars`
