# bin/bash

SUB_GIT_DIR=sub_projects

ANSIBLE_NAS_PROJECT_DIR_NAME=ansible_nas
ANSIBLE_NAS_GIT_URL=git@github.com:bVdCreations/ansible-nas.git
ANSIBLE_NAS_GIT_COMMIT=62111605753dfa6f140df21a04c905569b404817


K3S_ANSIBLE_PROJECT_DIR_NAME=k3s_ansible
K3S_ANSIBLE_GIT_URL=git@github.com:bVdCreations/k3s-ansible.git
K3S_ANSIBLE_GIT_COMMIT=8e7081243b4ffa83beffe53b58458824a00d1a38

 if [[ -d  $SUB_GIT_DIR ]]; then
   echo "folder for subprojects already exits"
 else
   echo "creating folder $SUB_GIT_DIR"
   mkdir $SUB_GIT_DIR
 fi


 if [[ -d  $SUB_GIT_DIR/$ANSIBLE_NAS_PROJECT_DIR_NAME ]]; then
   echo "$ANSIBLE_NAS_PROJECT_DIR_NAME already exists"
 else
   echo "cloning $ANSIBLE_NAS_PROJECT_DIR_NAME"
   git clone $ANSIBLE_NAS_GIT_URL $SUB_GIT_DIR/$ANSIBLE_NAS_PROJECT_DIR_NAME
   cd $SUB_GIT_DIR/$ANSIBLE_NAS_PROJECT_DIR_NAME 
   git checkout $ANSIBLE_NAS_GIT_COMMIT
   cd ../..
 fi

 if [[ -d  $SUB_GIT_DIR/$K3S_ANSIBLE_PROJECT_DIR_NAME ]]; then
   echo "$K3S_ANSIBLE_PROJECT_DIR_NAME already exists"
 else
   echo "cloning $K3S_ANSIBLE_PROJECT_DIR_NAME"
   git clone $K3S_ANSIBLE_GIT_URL $SUB_GIT_DIR/$K3S_ANSIBLE_PROJECT_DIR_NAME
   cd $SUB_GIT_DIR/$K3S_ANSIBLE_PROJECT_DIR_NAME
   git checkout $K3S_ANSIBLE_GIT_COMMIT
   cd ../..
 fi
