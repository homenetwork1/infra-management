# set label to node

example
'''bash
kubectl label nodes <your-node-name> foo=bar
'''

set labels
'''
kubectl label nodes kube-m010 disktype=usb
kubectl label nodes kube-n020 disktype=usb
kubectl label nodes kube-n030 disktype=usb
kubectl label nodes kube-n040-nuc disktype=ssd
kubectl label nodes kube-n020 node-role.kubernetes.io/worker=true
kubectl label nodes kube-n030 node-role.kubernetes.io/worker=true
kubectl label nodes kube-n040-nuc node-role.kubernetes.io/worker=true
'''
