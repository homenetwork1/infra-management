# ansible nas for proxmox

this project is forked from https://github.com/davestephens/ansible-nas  
the original project is for an ubuntu server.  
This fork will be based on the proxmox server which is based on debian

## setup

- first disable the proxmox interprise package from the rpoxmox web GUI (under Node)
- `make install-local`
- `make deb-update`
- `make apt-update`
- import zpool
- `make setup-rclone`
- `make setup-zfs`
- `make run`

### import zpool

run `zpool import`  
get the pool name  
run `zpool import -f <poolname>`

### environment variables for rclone

```
echo <backblaze_key_id> > ./.env/secrets/backblaze_backup_account
echo <backblaze_application_key> > ./.env/secrets/backblaze_backup_key
echo <backblaze_key_id> > ./.env/secrets/backblaze_media_account
echo <backblaze_application_key> > ./.env/secrets/backblaze_media_key
echo <crypt_password> > ./.env/secrets/crypt_backup_password
echo <crypt_password2> > ./.env/secrets/crypt_backup_password2
echo <crypt_password> > ./.env/secrets/crypt_media_password
echo <crypt_password2> > ./.env/secrets/crypt_media_password2
```
