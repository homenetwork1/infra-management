# debug k3s

https://docs.k3s.io/faq

## check k3s config

`k3s check-config`

## systemd

check file in systemd exists
`cat /etc/systemd/system/k3s-node.service`

```
systemctl status k3s-node
systemctl start k3s-node
```

check logs
`journalctl -fu k3s-node.service`

## firewall

check open ports from external
`nmap <ip>`
