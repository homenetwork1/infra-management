# Format application drive

This is a guide how to format the application drive where we store all the docker application data

## inspect

See all the datablock on the server

```
lsblk
```

See all the disk connected to the server

```
fdisk -l
```

## create partition table

watch https://www.youtube.com/watch?v=2Z6ouBYfZr8

```
fdisk /dev/<disk name>
# see help: m
# print the partisions: p   print the partition table
# create a partition label with : g create a new empty GPT partition table
# add a new partition: n   add a new partition
```

## format the partition

watch https://www.youtube.com/watch?v=2Z6ouBYfZr8

```
mkfs.btrfs -L application-drive /dev/partition
# example mkfs.btrfs -L application-drive /dev/sde1
```
