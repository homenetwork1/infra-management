# prepare BTRFS drive for docker application drive

video: https://www.youtube.com/watch?v=RPO-fS6HQbY

## Format drive to brtfs

see docs/disk_formatting

## mount drive

```
mkdir /mnt/application-data
mount /dev/<sd device> /mnt/application-data
df -h
```

## create subvolumes

```
btrfs subvolume create /mnt/application-data/docker
btrfs subvolume create /mnt/application-data/docker/application_configs
btrfs subvolume list /mnt/application-data
btrfs subvolume show /mnt/application-data/docker
btrfs subvolume show /mnt/application-data/docker/application_configs
```

## mount subvolumes

find uuid of drive

```
ls -alt /dev/disk/by-uuid/
# or
blkid /dev/sda1
```

backup fstab

```
cp /etc/fstab /etc/fstab.backup
```

append lines to subvolume

```
UUID=<uuid> /mnt/docker                     btrfs   subvol=docker,compress=zstd:1 0 0
```
