## list zfs volumes

```bash
zfs list
```

## cleanup snapshots

```bash
zfs list -t snapshot -o name | grep Series | tac | tail -n +16 | xargs -n 1 zfs destroy -r
```
