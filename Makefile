# project setup
setup_sub_projects:
	bash ./commands/setup_sub_projects.sh
install-local:
	ansible-galaxy install -r requirements.yaml

# firewall
load-unbound-config:
	bash ./commands/download-config-unbound.sh
felix_fire_setup:
	ansible-playbook playbooks/felix_fire/setup.yaml

# kodi
run_kodi:
	ansible-playbook playbooks/kodi/main.yaml
load_kodi_config:
	bash ./commands/get_kodi_config.sh

# nas
nas_playbook_run:
	ansible-playbook playbooks/nas/main.yaml
nas_apt_update:
	ansible-playbook playbooks/nas/update_apt.yaml
nas_deb_update:
	ansible-playbook playbooks/nas/deb_packages.yaml
nas_setup_zfs:
	ansible-playbook playbooks/nas/setup_zfs.yaml
nas_setup_rclone:
	ansible-playbook playbooks/nas/setup_rclone.yaml
nas_setup_rsync_pull:
	ansible-playbook playbooks/nas/setup_rsync_pull.yaml

# cluster
cluster_playbook_run:
	ansible-playbook playbooks/cluster/main.yaml
cloud-cluster_playbook_run:
	ansible-playbook playbooks/cluster/cloud_main.yaml

# localhost
application_run_localy:
	ansible-playbook  playbooks/localhost/nas_applications.yaml
application_stop_localy:
	ansible-playbook -e "{application_enabled: false}"  playbooks/localhost/nas_applications.yaml

dmz_playbook_run:
	ansible-playbook playbooks/dmz/main.yaml
