terraform {
  backend "s3" {
    bucket = "home-cloud-terraform-bucket"
    key    = "opentofu"
    region = "eu-west-3"
  }
  required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "~> 1.46"
    }
  }
}

# Set the variable value in *.tfvars file
# or using the -var="hcloud_token=..." CLI option
variable "hcloud_token" {
  sensitive = true
}

# Configure the Hetzner Cloud Provider
provider "hcloud" {
  token = var.hcloud_token
}

data "hcloud_ssh_keys" "root_ssh_key" {
  # labels are set in the hetzner console
  with_selector = "key=primary"
}

resource "hcloud_network" "private_k8s_network" {
  name     = "kubernetes-cluster"
  ip_range = "10.0.0.0/16"
}

resource "hcloud_network_subnet" "private_k8s_network_subnet" {
  type         = "cloud"
  network_id   = hcloud_network.private_k8s_network.id
  network_zone = "eu-central"
  ip_range     = "10.0.1.0/24"
}

resource "hcloud_server" "controler-node" {
  name        = "controler-node"
  image       = "debian-12"
  server_type = "cax11"
  datacenter  = "hel1-dc2"
  ssh_keys = data.hcloud_ssh_keys.root_ssh_key.ssh_keys.*.name
  labels = {
    "terraform" : "true"
  }
  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }
  network {
    network_id = hcloud_network.private_k8s_network.id
    # IP Used by the master node, needs to be static
    # Here the worker nodes will use 10.0.1.1 to communicate with the master node
    ip         = "10.0.1.1"
  }
  user_data = file("${path.module}/cloud-init.yaml")

  depends_on = [hcloud_network_subnet.private_k8s_network_subnet]
}

resource "hcloud_server" "worker-nodes" {
  count       = 1
  name        = "worker-node-${count.index}"
  image       = "debian-12"
  server_type = "cax11"
  datacenter  = "hel1-dc2"
  ssh_keys = data.hcloud_ssh_keys.root_ssh_key.ssh_keys.*.name
  labels = {
    "terraform" : "true"
  }
  public_net {
    ipv4_enabled = true
    ipv6_enabled = true
  }
  network {
    network_id = hcloud_network.private_k8s_network.id
  }
  user_data = file("${path.module}/cloud-init.yaml")

  depends_on = [hcloud_network_subnet.private_k8s_network_subnet, hcloud_server.controler-node]
}
